%========================================================================%
% triple_cart.m
% Author: George Xian
% Purpose: Simulates the three cart system
%========================================================================%

clear all
clc

%% System parameters
m = [1.608; 0.75; 0.75];    % cart masses (kg)
k = 175;                    % spring constant (N/m)
c = [0; 3.68; 3.68];        % cart dampings (Ns/m)

a = 12.45;                  % experimental constant
km = 0.00176;               % back emf
kg = 3.71;                  % gear ratio
R = 1.4;                    % resistance (ohms)
r = 0.0184;                 % pinion radius (m)

%% Generate matrices
masses = diag(m);
springs = k*[1, -1, 0; -1, 2, -1; 0, -1, 1];
damps = diag(c);

A = [zeros(3), eye(3); -masses\springs, -masses\damps];
A(4, 4) = A(4, 4) - (km^2*kg^2/(R*r^2))/m(1);      % apply back emf to first cart's velocity

B = [zeros(3, 1); (a*km*kg/(R*r))/m(1); 0; 0]; % co-efficient to represent force as voltage
C = [eye(3), zeros(3)];                 % full state feedback

%% Find controller gains
% Pole placement
outC = [0, 0, 1, 0, 0, 0];    % last cart pos 
Kpp = place(A, B, [-9, -8, -12, -10, -7, -11]);
Acl_pp = A - B*Kpp;
Acl_B_pp = Acl_pp\B;
Npp = -(outC*Acl_B_pp)^(-1);
%{
fprintf('Pole placed N = %.2f\n', Npp);
fprintf('Pole placed gain = %.2f\n', Kpp);
%}

% LQR

Q1 = eye(6);
Q1(3, 3) = 1000;
Q1(6, 6) = 10;
Klqr = lqr(A, B, Q1, 1);
Acl_lqr = A - B*Klqr;
Acl_B_lqr = Acl_lqr\B;
Nlqr = -(outC*Acl_B_lqr)^(-1);
fprintf('LQR n = %.2f\n', Nlqr);
fprintf('LQR gain = %.2f\n', Klqr); 

Q2 = eye(6);
Q2(3, 3) = 1000;
Q2(6, 6) = 100;
Klqr2 = lqr(A, B, Q2, 1);
Acl_lqr2 = A - B*Klqr2;
Acl_B_lqr2 = Acl_lqr2\B;
Nlqr2 = -(outC*Acl_B_lqr2)^(-1);
fprintf('LQR2 n = %.2f\n', Nlqr2);
fprintf('LQR2 gain = %.2f\n', Klqr2);

Q3 = eye(6);
Q3(3, 3) = 4000;
Q3(6, 6) = 100;
Klqr3 = lqr(A, B, Q3, 1);
Acl_lqr3 = A - B*Klqr3;
Acl_B_lqr3 = Acl_lqr3\B;
Nlqr3 = -(outC*Acl_B_lqr3)^(-1);
fprintf('LQR3 n = %.2f\n', Nlqr3);
fprintf('LQR3 gain = %.2f\n', Klqr3); 


%% System creation

sys_pp_y = ss(Acl_pp, Npp*B, outC, 0);
sys_pp_u = ss(Acl_pp, Npp*B, -Kpp, 0);

sys_lqr_y = ss(Acl_lqr, Nlqr*B, outC, 0);
sys_lqr_u = ss(Acl_lqr, Nlqr*B, -Klqr, 0);

sys_lqr2_y = ss(Acl_lqr2, Nlqr2*B, outC, 0);
sys_lqr2_u = ss(Acl_lqr2, Nlqr2*B, -Klqr2, 0);

sys_lqr3_y = ss(Acl_lqr3, Nlqr3*B, outC, 0);
sys_lqr3_u = ss(Acl_lqr3, Nlqr3*B, -Klqr3, 0);

%% Plot simulation
t = 0:0.05:10;
reference = 0.25*heaviside(t);

[y, t] = lsim(sys_pp_y, reference, t); 
[u, t] = lsim(sys_pp_u, reference, t); 
u = u + Npp*reference';
%{
figure(1);
subplot(2, 1, 1);
plot(t, y);
title('Output')
subplot(2, 1, 2);
plot(t, u);
title('Control Effort') 
%}

[ylqr, t] = lsim(sys_lqr_y, reference, t);
[ulqr, t] = lsim(sys_lqr_u, reference, t);
ulqr = ulqr + Nlqr*reference';
figure(2);
subplot(2, 1, 1);
plot(t, ylqr);
title('Output (LQR)')
subplot(2, 1, 2);
plot(t, ulqr);
title('Control Effort (LQR)')

[ylqr2, t] = lsim(sys_lqr2_y, reference, t);
[ulqr2, t] = lsim(sys_lqr2_u, reference, t);
ulqr2 = ulqr2 + Nlqr2*reference';
figure(3);
subplot(2, 1, 1);
plot(t, ylqr2);
title('Output (LQR2)')
subplot(2, 1, 2);
plot(t, ulqr2);
title('Control Effort (LQR2)')

[ylqr3, t] = lsim(sys_lqr3_y, reference, t);
[ulqr3, t] = lsim(sys_lqr3_u, reference, t);
ulqr3 = ulqr3 + Nlqr3*reference';
figure(4);
subplot(2, 1, 1);
plot(t, ylqr3);
title('Output (LQR3)')
subplot(2, 1, 2);
plot(t, ulqr3);
title('Control Effort (LQR3)')



